/*
 * board.h
 *
 * Calixto AM335x SOM boards information header
 *
 * Copyright (C) 2015, Calixto Systems Pvt Ltd - http://www.calixto.co.in/
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#ifndef _BOARD_H_
#define _BOARD_H_

void enable_uart0_pin_mux(void);
void enable_board_pin_mux(void);
#endif
